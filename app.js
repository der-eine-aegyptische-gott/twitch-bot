require('dotenv').config()
const tmi = require('tmi.js');

// Define configuration options
const opts = {
    identity: {
        username: process.env.BOT_USERNAME,
        password: process.env.OAUTH_TOKEN
    },
    channels: [
        process.env.CHANNEL_NAME1,
        process.env.CHANNEL_NAME2,
        process.env.CHANNEL_NAME3,
    ]
};

let deathCounter = [];
// Create a client with our options
const client = new tmi.client(opts);

// Register our event handlers (defined below)
client.on('message', onMessageHandler);
client.on('connected', onConnectedHandler);

// Connect to Twitch:
client.connect();

// Called every time a message comes in
function onMessageHandler (target, context, msg, self) {
    if (self) { return; } // Ignore messages from the bot

    console.log(context);
    // Remove whitespace from chat message
    const commandName = msg.trim();

    // If the command is known, let's execute it
    if(commandName === '!died') {
        if(!deathCounter[context['room-id']]) {
            deathCounter[context['room-id']] = 1
        } else {
            deathCounter[context['room-id']] += 1;
        }
        client.say(target,`Death count set to: ${deathCounter[context['room-id']]}.`);
    } else if(commandName === '!deaths') {
        client.say(target,`Streamer died ${deathCounter[context['room-id']]} times.`);
        console.log(`${deathCounter[context['room-id']]}`);
    } else {
        console.log(`* Unknown command ${commandName}`);
    }
}

// Called every time the bot connects to Twitch chat
function onConnectedHandler (addr, port) {
    console.log(`* Connected to ${addr}:${port}`);
}
